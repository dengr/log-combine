package ru.sportmaster.log_combine;

import java.net.Inet4Address;
import java.time.OffsetDateTime;
import java.util.Objects;

public class LogRecord implements Comparable<LogRecord> {

    private final Inet4Address remoteAddress;
    private final OffsetDateTime dateTime;
    private final String content;

    public LogRecord(Inet4Address remoteAddress, OffsetDateTime dateTime, String content) {
        this.remoteAddress = Objects.requireNonNull(remoteAddress);
        this.dateTime = Objects.requireNonNull(dateTime);
        this.content = Objects.requireNonNull(content);
    }

    public Inet4Address getRemoteAddress() {
        return remoteAddress;
    }

    public OffsetDateTime getDateTime() {
        return dateTime;
    }

    public String getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LogRecord logRecord = (LogRecord) o;
        return content.equals(logRecord.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(content);
    }

    @Override
    public String toString() {
        return content;
    }

    @Override
    public int compareTo(LogRecord o) {
        byte[] ipAddressAsBytes1 = getRemoteAddress().getAddress();
        byte[] ipAddressAsBytes2 = o.getRemoteAddress().getAddress();

        // и logRecord1, и logRecord2 имеют одну длину, поэтому можем так делать
        for (int i = 0; i < ipAddressAsBytes1.length; i++) {
            int b1 = byteToInt(ipAddressAsBytes1[i]);
            int b2 = byteToInt(ipAddressAsBytes2[i]);
            if (b1 == b2) {
                continue;
            }

            if (b1 < b2) {
                return -1;
            } else {
                return 1;
            }
        }

        return getDateTime().compareTo(o.getDateTime());
    }

    // преобразует byte в int в диапазоне 0-255 (8 бит)
    private static int byteToInt(byte b) {
        return (int) b & 0xFF;
    }
}

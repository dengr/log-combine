package ru.sportmaster.log_combine;

import java.net.Inet4Address;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Разбирает строку nginx access-log в формате combined.
 * Формат:
 * {@code
 * $remote_addr - $remote_user [$time_local] "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent"
 * }
 *
 * @see <a href="https://nginx.org/ru/docs/http/ngx_http_log_module.html#access_log">Nginx Combined Log Format</a>
 */
public class Parser {

    private static final Pattern LOG_FORMAT = Pattern.compile("(?<remoteAddr>[\\d.]+)\\s-\\s[^\\s]+\\s\\[(?<timeLocal>[^]]+)].+");
    private static final String REMOTE_ADDRESS_REGEX_KEY = "remoteAddr";
    private static final String TIME_LOCAL_REGEX_KEY = "timeLocal";

    private static final DateTimeFormatter TIME_LOCAL_FORMATTER = DateTimeFormatter.ofPattern("dd/MMM/yyyy:HH:mm:ss Z", Locale.US);

    private static final String EMPTY_INPUT__MSG = "Переданное значение не может быть пустым";
    private static final String INVALID_FORMAT__MSG = "Переданное значение %s должно быть в формате combined https://nginx.org/ru/docs/http/ngx_http_log_module.html#access_log";
    private static final String INVALID_REMOTE_ADDR_VALUE__MSG = "В переданном значении %s поле $remote_addr должно содержать корректное значение ipv4-адреса";
    private static final String INVALID_TIME_LOCAL_VALUE__MSG = "В переданном значении %s поле $time_local должно содержать корректное значение даты";

    /**
     * Разбирает строку лога.
     *
     * @param logRow строка лога
     * @return разобранная строка лога
     * @throws IllegalArgumentException, если строка не может быть разобрана
     * @see LogRecord
     */
    public static LogRecord parse(String logRow) throws IllegalArgumentException {
        if (logRow == null || logRow.isBlank()) {
            throw new IllegalArgumentException(EMPTY_INPUT__MSG);
        }

        var matcher = LOG_FORMAT.matcher(logRow);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(String.format(INVALID_FORMAT__MSG, logRow));
        }

        Inet4Address remoteAddress;
        try {
            remoteAddress = Utils.parseInet4Address(matcher.group(REMOTE_ADDRESS_REGEX_KEY));
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format(INVALID_REMOTE_ADDR_VALUE__MSG, logRow), ex);
        }

        OffsetDateTime dateTime;
        try {
            dateTime = OffsetDateTime.parse(matcher.group(TIME_LOCAL_REGEX_KEY), TIME_LOCAL_FORMATTER);
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format(INVALID_TIME_LOCAL_VALUE__MSG, logRow), ex);
        }

        return new LogRecord(remoteAddress, dateTime, logRow);
    }

    private Parser() {
    }
}

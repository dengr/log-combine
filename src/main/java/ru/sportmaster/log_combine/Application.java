package ru.sportmaster.log_combine;

import java.util.logging.Level;
import java.util.logging.Logger;

import static ru.sportmaster.log_combine.Exporter.export;
import static ru.sportmaster.log_combine.Loader.load;

public class Application {

    private static final Logger log = Logger.getLogger("log-combine");

    public static void main(String[] args) {
        try {
            var loadedLogRows = load(args);

            var keeper = new Keeper();
            loadedLogRows.stream().map(Parser::parse).forEach(keeper::take);

            export(keeper.handOver());
        } catch (Exception ex) {
            log.log(Level.SEVERE, ex.getMessage());
        }
    }
}

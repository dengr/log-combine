package ru.sportmaster.log_combine;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Выгружает логи.
 */
public class Exporter {

    private static final String EMPTY_INPUT__MSG = "Список логов не может быть null";
    private static final String FILE_NAME_FORMAT = "combined_access_%s.log";

    /**
     * Выгружает логи в файл.
     *
     * @param records логи для выгрузки
     * @throws UncheckedIOException, если произошла ошибка при выгрузке в файл
     */
    public static void export(List<LogRecord> records) throws IllegalArgumentException, UncheckedIOException {
        if (records == null) {
            throw new IllegalArgumentException(EMPTY_INPUT__MSG);
        }

        if (records.isEmpty()) {
            return;
        }

        try {
            Files.write(Path.of(String.format(FILE_NAME_FORMAT, System.currentTimeMillis())),
                    records.stream()
                            .map(LogRecord::getContent)
                            .collect(Collectors.toList()), StandardCharsets.UTF_8, StandardOpenOption.CREATE_NEW);
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    private Exporter() {
    }
}

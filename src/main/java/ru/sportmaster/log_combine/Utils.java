package ru.sportmaster.log_combine;

import java.net.Inet4Address;

public final class Utils {

    private static final String IP_ADDR_EMPTY_INPUT__MSG = "Переданное значение не может быть пустым";
    private static final String IP_ADDR_INVALID_INPUT__MSG = "Переданное значение не является корректным строковым литералом ipv4-адреса";

    public static Inet4Address parseInet4Address(String string) throws IllegalArgumentException {
        if (string == null || string.isBlank()) {
            throw new IllegalArgumentException(IP_ADDR_EMPTY_INPUT__MSG);
        }

        var ipAddressAsStringArray = string.split("\\.");
        if (ipAddressAsStringArray.length != 4 || !String.join(".", ipAddressAsStringArray).equals(string)) {
            throw new IllegalArgumentException(IP_ADDR_INVALID_INPUT__MSG);
        }

        var remoteAddressAsByteArray = new byte[4];
        for (var i = 0; i < ipAddressAsStringArray.length; i++) {
            var octet = Integer.parseInt(ipAddressAsStringArray[i]);
            if (octet > 255 || (ipAddressAsStringArray[i].startsWith("0") && ipAddressAsStringArray[i].length() > 1)) {
                throw new IllegalArgumentException(IP_ADDR_INVALID_INPUT__MSG);
            }

            remoteAddressAsByteArray[i] = (byte) octet;
        }

        try {
            return (Inet4Address) Inet4Address.getByAddress(remoteAddressAsByteArray);
        } catch (Exception ex) {
            throw new IllegalArgumentException(IP_ADDR_INVALID_INPUT__MSG, ex);
        }
    }

    private Utils() {
    }
}

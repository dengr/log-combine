package ru.sportmaster.log_combine;

import java.math.BigInteger;
import java.net.Inet4Address;
import java.util.*;

/**
 * Накапливает записи лога.
 */
public class Keeper {

    private final List<LogRecord> records;
    private final Map<Inet4Address, BigInteger> ipv4Frequency;

    /**
     * Инициализирует новое хранилище для накопления записей логов.
     */
    public Keeper() {
        records = new LinkedList<>();
        ipv4Frequency = new HashMap<>();
    }

    /**
     * Принять запись лога
     *
     * @param record запись лога
     */
    public void take(LogRecord record) {
        records.add(record);
        ipv4Frequency.merge(record.getRemoteAddress(), BigInteger.ONE, BigInteger::add);
    }

    /**
     * Возвращает все накопленные записи логов.
     *
     * @return накопленные записи логов
     */
    public List<LogRecord> handOver() {
        records.sort(new LogRecordsComparatorByIpv4Frequency());

        return Collections.unmodifiableList(records);
    }

    /**
     * Сравнивает записи лога по частоте использования $remote_addr
     */
    private class LogRecordsComparatorByIpv4Frequency implements Comparator<LogRecord> {

        @Override
        public int compare(LogRecord o1, LogRecord o2) {
            var frequency1 = ipv4Frequency.getOrDefault(o1.getRemoteAddress(), BigInteger.ONE);
            var frequency2 = ipv4Frequency.getOrDefault(o2.getRemoteAddress(), BigInteger.ONE);

            var frequencyCompareResult = frequency2.compareTo(frequency1);
            return frequencyCompareResult != 0 ? frequencyCompareResult : o1.compareTo(o2);
        }
    }
}

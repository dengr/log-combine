package ru.sportmaster.log_combine;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Загружает файлы на обработку.
 */
public class Loader {

    private static final String EMPTY_INPUT__MSG = "Список файлов не может быть null";
    private static final String NOT_EXISTS_FILE__MSG = "Не удалось прочитать файл %s. Проверьте его существование или права доступа к нему";

    /**
     * Загружает файлы на обработку.
     *
     * @param files файлы
     * @return список строк из файлов
     * @throws IllegalArgumentException, если список файлов null
     * @throws UncheckedIOException,     если произошла ошибка при работе с файлами
     */
    public static List<String> load(String... files) throws IllegalArgumentException, UncheckedIOException {
        if (files == null) {
            throw new IllegalArgumentException(EMPTY_INPUT__MSG);
        }

        var readRows = new ArrayList<String>();
        for (String file : files) {
            if (file == null) {
                continue;
            }

            var filePath = Path.of(file);

            if (Files.notExists(filePath, LinkOption.NOFOLLOW_LINKS)) {
                throw new UncheckedIOException(new IOException(String.format(NOT_EXISTS_FILE__MSG, file)));
            }

            try {
                readRows.addAll(Files.readAllLines(filePath));
            } catch (IOException ex) {
                throw new UncheckedIOException(ex);
            }
        }

        return Collections.unmodifiableList(readRows);
    }

    private Loader() {
    }
}

package ru.sportmaster.log_combine;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.UncheckedIOException;

import static org.junit.jupiter.api.Assertions.*;

class LoaderTest {


    @DisplayName("null в качестве аргумента будет вызывать исключение")
    @Test
    void testLoadNullThrowsException() {
        assertThrows(IllegalArgumentException.class, () -> Loader.load((String[]) null));
    }

    @DisplayName("если в списке есть null элемент, то он будет пропущен")
    @Test
    void testLoadedNullElementWillSkipped() {
        assertTrue(Loader.load(new String[]{null}).isEmpty());
    }

    @DisplayName("чтение файла, которого нет, будет вызывать исключение")
    @Test
    void testLoadNotExistFileThrowsException() {
        assertThrows(UncheckedIOException.class, () -> Loader.load("/t/m/p/file.log"));
    }

    @DisplayName("чтение файла, которого нет, будет вызывать исключение и не даст другим файлам быть прочитанными")
    @Test
    void testLoadNotExistFileBreaksLoadProcess() throws Exception {
        var file = getClass().getResource("/logs/access.log").toURI().toString();
        var notExistsFile = "/t/m/p/file.log";
        assertThrows(UncheckedIOException.class, () -> Loader.load(file, notExistsFile));
    }

    @DisplayName("существующие файлы (и без ограничений) будут прочитаны")
    @Test
    void testLoadExistsFiles() throws Exception {
        var strings = Loader.load(
                new File(getClass().getResource("/logs/access.log").toURI()).getAbsolutePath(),
                new File(getClass().getResource("/logs/access.log.0").toURI()).getAbsolutePath()
        );
        assertEquals(8, strings.size());
    }
}

package ru.sportmaster.log_combine;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LogRecordComparisonTest {

    @DisplayName("Тестирование без учёта частоты использования $remote_addr")
    @Nested
    class SimpleSorting {

        @DisplayName("произвольные список сообщений с разными значениями $remote_addr будет упорядочен по возрастанию октетов ipv4-адреса")
        @Test
        void testSortByRemoteAddress() {
            var r1 = "74.86.158.106 - - [24/Mar/2014:06:33:04 +0100] ";
            var r2 = "223.40.215.23 - - [24/Mar/2014:06:33:04 +0100] ";
            var r3 = "14.234.213.29 - - [24/Mar/2014:06:33:04 +0100] ";
            var r4 = "1.198.3.93 - - [24/Mar/2014:06:33:04 +0100] ";
            var r5 = "32.183.93.40 - - [24/Mar/2014:06:33:04 +0100] ";
            var r6 = "104.214.244.2 - - [24/Mar/2014:06:33:04 +0100] ";
            var r7 = "104.214.4.1 - - [24/Mar/2014:06:33:04 +0100] ";
            var logRows = List.of(r1, r2, r3, r4, r5, r6, r7);

            var logRecords = logRows.stream().map(Parser::parse).collect(Collectors.toList());
            Collections.shuffle(logRecords);
            Collections.sort(logRecords);

            assertEquals(r4, logRecords.get(0).getContent());
            assertEquals(r3, logRecords.get(1).getContent());
            assertEquals(r5, logRecords.get(2).getContent());
            assertEquals(r1, logRecords.get(3).getContent());
            assertEquals(r7, logRecords.get(4).getContent());
            assertEquals(r6, logRecords.get(5).getContent());
            assertEquals(r2, logRecords.get(6).getContent());
        }

        @DisplayName("список сообщений с одинаковыми значениями $remote_addr будет упорядочен по возрастанию значения поля $time_local")
        @Test
        void testSortByTimeLocal() {
            var logRecord1 = Parser.parse("74.86.158.106 - - [24/Jun/2014:08:13:04 +0100] ");
            var logRecord2 = Parser.parse("74.86.158.106 - - [24/Mar/2014:06:33:04 +0100] ");

            var logRecords = new ArrayList<>(List.of(logRecord1, logRecord2));
            Collections.shuffle(logRecords);
            Collections.sort(logRecords);

            assertEquals(logRecord1, logRecords.get(1));
            assertEquals(logRecord2, logRecords.get(0));
        }
    }

    @DisplayName("Тестирование с учётом частоты использования $remote_addr")
    @Nested
    class SortingWithIpv4Frequency {

        @DisplayName("если встречаются повторяющиеся значения $remote_addr, то сортировка сначала будет идти по частоте использования $remote_addr")
        @Test
        void testSortWithDuplicateAddresses() {
            var r1 = "74.86.158.106 - - [24/Mar/2014:06:33:04 +0100] ";
            var r2 = "1.198.3.93 - - [24/Mar/2014:06:33:04 +0100] ";
            var r3 = "104.214.4.1 - - [24/Mar/2016:06:33:04 +0100] ";
            var r4 = "223.40.215.23 - - [24/Mar/2014:06:33:04 +0100] ";
            var r5 = "14.234.213.29 - - [24/Mar/2014:06:33:04 +0100] ";
            var r6 = "74.86.158.106 - - [24/Mar/2014:06:35:04 +0100] ";
            var r7 = "32.183.93.40 - - [24/Mar/2014:06:33:04 +0100] ";
            var r8 = "104.214.244.2 - - [24/Mar/2014:06:33:04 +0100] ";
            var r9 = "74.86.158.106 - - [24/Mar/2014:06:38:04 +0100] ";
            var r10 = "104.214.4.1 - - [24/Mar/2016:06:43:04 +0100] ";
            var logRows = List.of(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10);

            var keeper = new Keeper();
            logRows.stream().map(Parser::parse).forEach(keeper::take);
            var logRecords = keeper.handOver();

            assertEquals(r1, logRecords.get(0).getContent());
            assertEquals(r6, logRecords.get(1).getContent());
            assertEquals(r9, logRecords.get(2).getContent());
            assertEquals(r3, logRecords.get(3).getContent());
            assertEquals(r10, logRecords.get(4).getContent());
            assertEquals(r2, logRecords.get(5).getContent());
            assertEquals(r5, logRecords.get(6).getContent());
            assertEquals(r7, logRecords.get(7).getContent());
            assertEquals(r8, logRecords.get(8).getContent());
            assertEquals(r4, logRecords.get(9).getContent());
        }
    }

    @DisplayName("если нет повторяющихся значений $remote_addr, то сортировка будет идти только между LogRecord")
    @Test
    void testSortWithOnlyUniqueAddresses() {
        var r1 = "74.86.158.106 - - [24/Mar/2014:06:33:04 +0100] ";
        var r2 = "1.198.3.93 - - [24/Mar/2014:06:33:04 +0100] ";
        var r3 = "104.214.4.1 - - [24/Mar/2016:06:33:04 +0100] ";
        var r4 = "223.40.215.23 - - [24/Mar/2014:06:33:04 +0100] ";
        var r5 = "14.234.213.29 - - [24/Mar/2014:06:33:04 +0100] ";
        var r6 = "32.183.93.40 - - [24/Mar/2014:06:33:04 +0100] ";
        var r7 = "104.214.244.2 - - [24/Mar/2014:06:33:04 +0100] ";
        var logRows = List.of(r1, r2, r3, r4, r5, r6, r7);

        var keeper = new Keeper();
        logRows.stream().map(Parser::parse).forEach(keeper::take);
        var logRecords = keeper.handOver();

        assertEquals(r2, logRecords.get(0).getContent());
        assertEquals(r5, logRecords.get(1).getContent());
        assertEquals(r6, logRecords.get(2).getContent());
        assertEquals(r1, logRecords.get(3).getContent());
        assertEquals(r3, logRecords.get(4).getContent());
        assertEquals(r7, logRecords.get(5).getContent());
        assertEquals(r4, logRecords.get(6).getContent());
    }
}

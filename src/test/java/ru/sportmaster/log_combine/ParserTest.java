package ru.sportmaster.log_combine;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ParserTest {

    @DisplayName("null в качестве аргумента будет вызывать исключение")
    @Test
    void testNullInputThrowsException() {
        assertThrows(IllegalArgumentException.class, () -> Parser.parse(null));
    }

    @DisplayName("пустая строка в качестве аргумента будет вызывать исключение")
    @Test
    void testBlankInputThrowsException() {
        assertThrows(IllegalArgumentException.class, () -> Parser.parse(""));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse(" "));
    }

    @DisplayName("сообщение не в формате nginx combined в качестве аргумента будет вызывать исключение")
    @Test
    void testInvalidLogFormatThrowsException() {
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("логи, но не nginx"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("74.86.158.106 - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("74.86.158.106 - - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("74.86.158.106 [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("74.86.158.106[24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("74.86.158.106 - - 24/Mar/2014:06:33:04 +0100 tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("74.86.158.106 - - tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("74.86.158.106 - - [] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("- - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse(" - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("24/Mar/2014:06:33:04 +0100 - - [74.86.158.106] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("74.86.158.106 - - [24/Mar/2014:06:33:04 +0100]"));
    }

    @DisplayName("некорректное значение $remote_addr будет вызывать исключение")
    @Test
    void testInvalidRemoteAddressIpv4ValueThrowsException() {
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("null - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("localhost - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("google.com - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("http://74.86.158.106 - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("7486158106 - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("127.1 - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("1.1.1.01 - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("74.86.158.10.6 - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("192.168.1.36.424.3232.1.0.3.0 - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("09.09.09.09 - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("9999.166.166.166.166 - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("... - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse(".....0.....1.2.3.0 - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("-1.2.3.4 - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("1.1.1.1. - - [24/Mar/2014:06:33:04 +0100] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("3...3 - - [24/Mar/2014:06:33:04 +0100] tail"));
    }

    @DisplayName("некорректное значение $time_local будет вызывать исключение")
    @Test
    void testInvalidTimeLocalValueThrowsException() {
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("74.86.158.106 - - [null] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("74.86.158.106 - - [ ] tail"));
        assertThrows(IllegalArgumentException.class, () -> Parser.parse("74.86.158.106 - - [240331040633040100] tail"));
        // fixme дописать тесты
    }

    @DisplayName("корректное значение будет распарсено")
    @Test
    void testParseValidValue() {
        var row = "74.86.158.106 - - [24/Mar/2014:06:33:04 +0100] \"GET /en HTTP/1.1\" 200 10589 \"-\" \"Mozilla/5.0+(compatible; UptimeRobot/2.0; http://www.uptimerobot.com/)\" \"-\"";
        var record = Parser.parse(row);
        assertEquals("74.86.158.106", record.getRemoteAddress().getHostAddress());
        assertEquals(OffsetDateTime.of(2014, 3, 24, 6, 33, 4, 0, ZoneOffset.ofHours(1)), record.getDateTime());
        assertEquals(record.getContent(), row);
    }
}
